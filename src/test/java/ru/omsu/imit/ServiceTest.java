package ru.omsu.imit;

import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class ServiceTest {

    @Test
    public void getDisciplineNamesTest() throws Exception {
        TableRow tableRow1 = new TableRow("матанализ", 155, Rating.FIVE);
        TableRow tableRow2 = new TableRow("алгем", 155, Rating.FIVE);
        TableRow tableRow3 = new TableRow("дискретная математика", 155, Rating.TWO);
        List<TableRow> tableRows = new ArrayList<>();
        tableRows.add(tableRow1);
        tableRows.add(tableRow2);
        tableRows.add(tableRow3);
        Certificate certificate = new Certificate("A B C", "OmSU",
                "Imit", "math",
                new GregorianCalendar(2015, Calendar.SEPTEMBER, 1),
                new GregorianCalendar(2019, Calendar.MAY, 31), tableRows);


        List<String> expected = new ArrayList<>();
        expected.add(tableRow1.getDisciplineName());
        expected.add(tableRow2.getDisciplineName());
        expected.add(tableRow3.getDisciplineName());

        List<String> result = Service.getDisciplineNames(certificate);

        Assert.assertEquals(expected, result);
    }

    @Test
    public void getSumOfLaborInputTest() throws Exception {
        TableRow tableRow1 = new TableRow("матанализ", 155, Rating.FIVE);
        TableRow tableRow2 = new TableRow("алгем", 100, Rating.FIVE);
        TableRow tableRow3 = new TableRow("дискретная математика", 130, Rating.TWO);
        List<TableRow> tableRows = new ArrayList<>();
        tableRows.add(tableRow1);
        tableRows.add(tableRow2);
        tableRows.add(tableRow3);
        Certificate certificate = new Certificate("A B C", "OmSU",
                "Imit", "math",
                new GregorianCalendar(2015, Calendar.SEPTEMBER, 1),
                new GregorianCalendar(2019, Calendar.MAY, 31), tableRows);

        int expected = 385;
        int result = Service.getSumOfLaborInput(certificate);

        Assert.assertEquals(expected, result);
    }

    @Test
    public void getRatingAverageTest() throws Exception {
        TableRow tableRow1 = new TableRow("матанализ", 155, Rating.FOUR);
        TableRow tableRow2 = new TableRow("алгем", 100, Rating.FIVE);
        TableRow tableRow3 = new TableRow("дискретная математика", 130, Rating.TWO);
        TableRow tableRow4 = new TableRow("ямп", 130, Rating.PASSED);
        List<TableRow> tableRows = new ArrayList<>();
        tableRows.add(tableRow1);
        tableRows.add(tableRow2);
        tableRows.add(tableRow3);
        tableRows.add(tableRow4);
        Certificate certificate = new Certificate("A B C", "OmSU",
                "Imit", "math",
                new GregorianCalendar(2015, Calendar.SEPTEMBER, 1),
                new GregorianCalendar(2019, Calendar.MAY, 31), tableRows);

        double expected = 4.5;
        double result = Service.getRatingAverage(certificate);

        Assert.assertEquals(expected, result, 1e-9);
    }

    @Test
    public void getMapDisciplineToRatingTest() throws Exception {
        TableRow tableRow1 = new TableRow("матанализ", 155, Rating.FOUR);
        TableRow tableRow2 = new TableRow("алгем", 100, Rating.FIVE);
        TableRow tableRow3 = new TableRow("дискретная математика", 130, Rating.TWO);
        TableRow tableRow4 = new TableRow("ямп", 130, Rating.PASSED);
        List<TableRow> tableRows = new ArrayList<>();
        tableRows.add(tableRow1);
        tableRows.add(tableRow2);
        tableRows.add(tableRow3);
        tableRows.add(tableRow4);
        Certificate certificate = new Certificate("A B C", "OmSU",
                "Imit", "math",
                new GregorianCalendar(2015, Calendar.SEPTEMBER, 1),
                new GregorianCalendar(2019, Calendar.MAY, 31), tableRows);

        Map<String, Rating> expected = new HashMap<>();
        expected.put("матанализ", Rating.FOUR);
        expected.put("алгем", Rating.FIVE);
        expected.put("дискретная математика", Rating.TWO);
        expected.put("ямп", Rating.PASSED);

        Map<String, Rating> result = Service.getMapDisciplineToRating(certificate);

        Assert.assertEquals(expected, result);
    }

    @Test
    public void getMapRatingToDisciplineListTest() throws Exception {
        TableRow tableRow1 = new TableRow("матанализ", 155, Rating.FIVE);
        TableRow tableRow2 = new TableRow("алгем", 100, Rating.FIVE);
        TableRow tableRow3 = new TableRow("дискретная математика", 130, Rating.TWO);
        TableRow tableRow4 = new TableRow("ямп", 130, Rating.PASSED);
        List<TableRow> tableRows = new ArrayList<>();
        tableRows.add(tableRow1);
        tableRows.add(tableRow2);
        tableRows.add(tableRow3);
        tableRows.add(tableRow4);
        Certificate certificate = new Certificate("A B C", "OmSU",
                "Imit", "math",
                new GregorianCalendar(2015, Calendar.SEPTEMBER, 1),
                new GregorianCalendar(2019, Calendar.MAY, 31), tableRows);

        Map<Rating, List<String>> expected = new HashMap<>();
        List<String> list1 = new ArrayList<>();
        list1.add("матанализ");
        list1.add("алгем");
        expected.put(Rating.FIVE, list1);
        List<String> list2 = new ArrayList<>();
        list2.add("дискретная математика");
        expected.put(Rating.TWO, list2);
        List<String> list3 = new ArrayList<>();
        list3.add("ямп");
        expected.put(Rating.PASSED, list3);

        Map<Rating, List<String>> result = Service.getMapRatingToDisciplineList(certificate);

        Assert.assertEquals(expected, result);
    }

}