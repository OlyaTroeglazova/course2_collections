package ru.omsu.imit;

import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class CertificateTest {

    @Test
    public void createCertificateTest() throws Exception {
        TableRow tableRow = new TableRow("матанализ", 155, Rating.FIVE);
        List<TableRow> tableRows = new ArrayList<>();
        tableRows.add(tableRow);
        Certificate certificate = new Certificate("A B C", "OmSU",
                "Imit", "math",
                new GregorianCalendar(2015, Calendar.SEPTEMBER, 1),
                new GregorianCalendar(2019, Calendar.MAY, 31), tableRows);
        Assert.assertEquals(certificate.getName(),"A B C");
        Assert.assertEquals(certificate.getUniversity(), "OmSU");
        Assert.assertEquals(certificate.getFaculty(), "Imit");
        Assert.assertEquals(certificate.getSpecialty(), "math");
        Assert.assertEquals(certificate.getStartDate(), new GregorianCalendar(2015, Calendar.SEPTEMBER, 1));
        Assert.assertEquals(certificate.getFinishDate(), new GregorianCalendar(2019, Calendar.MAY, 31));
        Assert.assertEquals(certificate.getTableRows(), tableRows);
    }

    @Test (expected = CertificateException.class)
    public void createCertificateWithWrongParamsTest1() throws Exception {
        TableRow tableRow = new TableRow("матанализ", 155, Rating.FIVE);
        List<TableRow> tableRows = new ArrayList<>();
        tableRows.add(tableRow);
        Certificate certificate = new Certificate("A B", "OmSU",
                "Imit", "math",
                new GregorianCalendar(2015, Calendar.SEPTEMBER, 1),
                new GregorianCalendar(2019, Calendar.MAY, 31), tableRows);
    }

    @Test (expected = CertificateException.class)
    public void createCertificateWithWrongParamsTest2() throws Exception {
        TableRow tableRow = new TableRow("матанализ", 155, Rating.FIVE);
        List<TableRow> tableRows = new ArrayList<>();
        tableRows.add(tableRow);
        Certificate certificate = new Certificate("A B C", "",
                "Imit", "math",
                new GregorianCalendar(2015, Calendar.SEPTEMBER, 1),
                new GregorianCalendar(2019, Calendar.MAY, 31), tableRows);
    }

    @Test (expected = CertificateException.class)
    public void createCertificateWithWrongParamsTest3() throws Exception {
        TableRow tableRow = new TableRow("матанализ", 155, Rating.FIVE);
        List<TableRow> tableRows = new ArrayList<>();
        tableRows.add(tableRow);
        Certificate certificate = new Certificate("A B C", "OmSU",
                "", "math",
                new GregorianCalendar(2015, Calendar.SEPTEMBER, 1),
                new GregorianCalendar(2019, Calendar.MAY, 31), tableRows);
    }

}