package ru.omsu.imit;

import java.util.Objects;

public class TableRow
{
    private String disciplineName; //название дисциплины
    private int laborInput; //трудоемкость
    private Rating rating; //оценка

    public TableRow(String disciplineName, int laborInput, Rating rating) throws Exception {
        this.setDisciplineName(disciplineName);
        this.laborInput = laborInput;
        this.rating = rating;
    }

    public String getDisciplineName() {
        return disciplineName;
    }

    public void setDisciplineName(String disciplineName) throws Exception {
        if(disciplineName.isEmpty()){
            throw new Exception("Wrong params");
        }
        this.disciplineName = disciplineName;
    }

    public int getLaborInput() {
        return laborInput;
    }

    public void setLaborInput(int laborInput) {
        this.laborInput = laborInput;
    }

    public Rating getRating() {
        return rating;
    }

    public void setRating(Rating rating) {
        this.rating = rating;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TableRow tableRow = (TableRow) o;
        return laborInput == tableRow.laborInput &&
                Objects.equals(disciplineName, tableRow.disciplineName) &&
                rating == tableRow.rating;
    }

    @Override
    public int hashCode() {
        return Objects.hash(disciplineName, laborInput, rating);
    }
}
