package ru.omsu.imit;

import java.util.Calendar;
import java.util.List;
import java.util.Objects;

public class Certificate {
    private String name; //ФИО
    private String university; //название университета
    private String faculty; //факультет
    private String specialty; //специальности
    private Calendar startDate; //дата начала
    private Calendar finishDate; //дата окончания
    private List<TableRow> tableRows; //строки таблицы

    public Certificate(String name, String university, String faculty, String specialty, Calendar startDate, Calendar finishDate, List<TableRow> tableRows) throws CertificateException {
        this.setName(name);
        this.setUniversity(university);
        this.setFaculty(faculty);
        this.setSpecialty(specialty);
        this.startDate = startDate;
        this.finishDate = finishDate;
        this.tableRows = tableRows;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws CertificateException {
        if(name.isEmpty() || name.split(" ").length != 3) {
            throw new CertificateException("Wrong Params");
        }
        this.name = name;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) throws CertificateException {
        if(university.isEmpty()) {
            throw new CertificateException("Wrong Params");
        }
        this.university = university;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) throws CertificateException {
        if(faculty.isEmpty()) {
            throw new CertificateException("Wrong Params");
        }
        this.faculty = faculty;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) throws CertificateException {
        if(specialty.isEmpty()) {
            throw new CertificateException("Wrong Params");
        }
        this.specialty = specialty;
    }

    public Calendar getStartDate() {
        return startDate;
    }

    public void setStartDate(Calendar startDate) {
        this.startDate = startDate;
    }

    public Calendar getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Calendar finishDate) {
        this.finishDate = finishDate;
    }

    public List<TableRow> getTableRows() {
        return tableRows;
    }

    public void setTableRows(List<TableRow> tableRows) {
        this.tableRows = tableRows;
    }

    public void setTableRow(TableRow tableRow) {
        this.tableRows.add(tableRow);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Certificate that = (Certificate) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(university, that.university) &&
                Objects.equals(faculty, that.faculty) &&
                Objects.equals(specialty, that.specialty) &&
                Objects.equals(startDate, that.startDate) &&
                Objects.equals(finishDate, that.finishDate) &&
                Objects.equals(tableRows, that.tableRows);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, university, faculty, specialty, startDate, finishDate, tableRows);
    }
}
