package ru.omsu.imit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Service {
    public static List<String> getDisciplineNames(Certificate certificate) {
        List<String> names = new ArrayList<>();
        for(TableRow tableRow: certificate.getTableRows()) {
            names.add(tableRow.getDisciplineName());
        }
        return names;
    }

    public static int getSumOfLaborInput(Certificate certificate) {
        int sum = 0;
        for(TableRow tableRow: certificate.getTableRows()) {
            sum+=tableRow.getLaborInput();
        }
        return sum;
    }

    public static double getRatingAverage(Certificate certificate) {
        int count = 0;
        double sum = 0;
        for(TableRow tableRow: certificate.getTableRows()) {
            if(tableRow.getRating().equals(Rating.THREE) ||
                    tableRow.getRating().equals(Rating.FOUR) || tableRow.getRating().equals(Rating.FIVE)) {
                sum+=tableRow.getRating().getValue();
                count++;
            }
        }
        return sum/count;
    }

    public static Map<String, Rating> getMapDisciplineToRating(Certificate certificate) {
        Map<String, Rating> map = new HashMap<>();
        for(TableRow tableRow: certificate.getTableRows()) {
            map.put(tableRow.getDisciplineName(), tableRow.getRating());
        }
        return map;
    }

    public static Map<Rating, List<String>> getMapRatingToDisciplineList(Certificate certificate) {
        Map<Rating, List<String>> map = new HashMap<>();
        for(TableRow tableRow: certificate.getTableRows()) {
            List<String> list;
            if(map.containsKey(tableRow.getRating())){
                list = map.get(tableRow.getRating());
            }else {
               list = new ArrayList<>();
            }
            list.add(tableRow.getDisciplineName());
            map.put(tableRow.getRating(), list);
        }
        return map;
    }
}
