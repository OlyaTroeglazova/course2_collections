package ru.omsu.imit;

public class CertificateException extends Exception{

    public CertificateException() {
    }

    public CertificateException(final String message) {
        super(message);
    }

    public CertificateException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public CertificateException(final Throwable cause) {
        super(cause);
    }
}
