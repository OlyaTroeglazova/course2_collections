package ru.omsu.imit;

public enum Rating {
    TWO(2),
    THREE(3),
    FOUR(4),
    FIVE(5),
    PASSED(1), //ЗАЧТЕНО
    NOT_PASSED(0); //НЕ ЗАЧТЕНО

    private final int value;

    Rating(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }



}
